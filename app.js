const express = require("express");
const bodyParser = require("body-parser");
const app = express();

const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const urlDB = 'mongodb://localhost:27017/test';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({"extended": true}));

const rtAPI = express.Router();
const ObjectId = mongodb.ObjectId;

rtAPI.get("/contacts/", function(req, res) {
    let query = Object.assign({}, req.query);
	getContacts(query)
		.then(contacts => res.json(contacts))
		.catch(error => console.error(error));
});

rtAPI.get("/contacts/:id", function(req, res) {
	getContact(req.params.id)
		.then(contact => {
			if(!contact) {
                return res.status(404).send();
            }
            res.json(contact);
		})
		.catch(error => {
			console.error(error);
			res.status(500).send();
		});
});

rtAPI.post("/contacts/", function(req, res) {
    if(!req.body) return res.sendStatus(400);
    let contact = Object.assign({}, req.body);
	let contacts = [contact];

	createContacts(contacts)
        .then(result => {
            res.json(result);
        })
        .catch(error => {
            console.error(error);
            res.status(500).send();
        });
});

rtAPI.put("/contacts/:id", function(req, res) {
	if(!req.body) return res.sendStatus(400);
	/*let fields = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		phone: req.body.phone
	};*/
    let fields = Object.assign({}, req.body);

    updateContact(req.params.id, fields)
        .then(contact => {
            if(!contact) {
                return res.status(404).send();
            }
            res.json(contact);
        })
        .catch(error => {
            console.error(error);
            res.status(500).send();
        });
});

rtAPI.delete("/contacts/:id", function(req, res) {
	deleteContact(req.params.id)
        .then(result => {
            res.json(result);
        })
        .catch(error => {
            console.error(error);
            res.status(500).send();
        });
});

app.use("/api/v1", rtAPI);

MongoClient.connect(urlDB)
    .then(db => {
        let collection = db.collection('contacts');
        return collection.find().count()
    })
    // Если коллекция пустая заполним ее тестовыми данными
    .then(count => {
		if (count == 0) {
			contacts = [
				{firstName: 'Георгий', lastName: 'Полтавченко', phone: '576-4501'},
				{firstName: 'Александр', lastName: 'Говорунов', phone: '576-6262'},
				{firstName: 'Игорь', lastName: 'Воскобойник', phone: '576-6318'},
				{firstName: 'Сергей', lastName: 'Борковской', phone: '(495) 695-7727'},
				{firstName: 'Валентин', lastName: 'Каблуков', phone: '576-7332'},
				{firstName: 'Игорь', lastName: 'Князев', phone: '576-7203'},
			]
			createContacts(contacts);
		}
	})
	.then(result => {
		app.listen(3000, () => { console.log("Ожидаем подключения")});
	})
	.catch(error => console.error(error));


function getContact(id) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(urlDB)
            .then(db => {
                let collection = db.collection('contacts');
                return collection.find({_id: new ObjectId(id)}).limit(1).nextObject();
            })
            .then(result => {
                resolve(result);
            })
            .catch(error => {
                reject(error);
            });
    });
}

function getContacts(query) {
	return new Promise((resolve, reject) => {
		MongoClient.connect(urlDB)
			.then(db => {
				let collection = db.collection('contacts');
				/*let plane = {};
				let limit = Number(query.limit);
				let offset = Number(query.offset);
				if (query.fields !== undefined) {
                    String(query.fields).split(",").forEach(elem => plane[elem] = 1);
				}
				let cursor = collection.find({}, plane);
				if (!isNaN(limit)) {
					cursor.limit(limit);
				}
				if (!isNaN(offset)) {
					cursor.skip(offset);
				}
				return cursor.toArray();*/
				return collection.find(query).toArray();
			})
			.then(result => {
				let contacts = result;
				resolve(contacts);
			})
			.catch(error => {
				reject(error);
			});
		});
}

function createContacts(contacts) {
	return new Promise((resolve, reject) => {
		MongoClient.connect(urlDB)
			.then(db => {
			  let collection = db.collection('contacts');
			  return collection.insert(contacts);
			})
			.then(result => {
				resolve(result.ops);
			})
			.catch(error => {
				reject(error);
			});
		});
}

function updateContact(id, fields) {
	return new Promise((resolve, reject) => {
		MongoClient.connect(urlDB)
			.then(db => {
			  	let collection = db.collection('contacts');
			  	//let contact = {'$set': fields};
                // чтобы иметь возможность удалить поля через наше api, обновлять будем исключительно по переданным данным
				let contact = fields;
                return collection.update({'_id': new ObjectId(id)}, contact);
            })
			.then(result => {
				resolve((result.result.n) ? getContact(id) : undefined);
			})
			.catch(error => {
				reject(error);
			});
		});
}

function deleteContact(id) {
	return new Promise((resolve, reject) => {
		MongoClient.connect(urlDB)
			.then(db => {
			  let collection = db.collection('contacts');
			  return collection.remove({'_id': new ObjectId(id)});
			})
			.then(result => {
				resolve(result);
			})
			.catch(error => {
				reject(error);
			});
		});
}

function deleteContacts() {
	return new Promise((resolve, reject) => {
		MongoClient.connect(urlDB)
			.then(db => {
			  let collection = db.collection('contacts');
			  return collection.remove();
			})
			.then(result => {
				resolve(result);
			})
			.catch(error => {
				reject(error);
			});
		});
}
